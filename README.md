# 微信公众号极速开发后台


<img src="http://7j1wfp.com1.z0.glb.clouddn.com/eova-weixin.png" width = "100%" alt="如果对你有帮助，请随意打赏支持" align=center />

## 后台源码

在[weixin_guide](http://git.oschina.net/javen205/weixin_guide) dev分支，后台使用的Eova开源框架搭建。

## 微信开发博客专栏

>如果你是初次接触微信开发请先浏览下我之前写的博客

【[CSDN专栏](http://blog.csdn.net/column/details/14826.html)】  【[简书专栏](http://www.jianshu.com/c/967739deb3e9)】

## 使用指南

> 简易版的文字介绍说明，后期会考虑录制视频(待定)

[简易的微信公众号管理平台使用指南](http://blog.csdn.net/zyw_java/article/details/73929080)



如发现`bug`或者有更好的建议或者意见请发`Issues`

## 功能列表

- 支持多公众号管理

- 关注自动回复(支持多消息回复)

1. 文本
2. 多图文
3. 图片
4. 红包

- 关键字回复(支持多消息回复)

1. 文本
2. 多图文
3. 图片
4. 红包
	
- 自定义菜单(支持个性化)

1. click（点击推事件）
2. view （跳转URL）
3. scancode_push （扫码推事件）
4. scancode_waitmsg（扫码推事件且弹出“消息接收中”提示框）
5. pic_sysphoto （弹出系统拍照发图）
6. pic_photo_or_album（弹出拍照或者相册发图）
7. pic_weixin （弹出微信相册发图器）
8. location_select （弹出地理位置选择器）
9. media_id （下发消息（除文本消息）
10. view_limited（跳转图文消息URL）
11. miniprogram （小程序 仅认证公众号可配置）
	
- 授权获取用户信息(支持多公众号)
- 实现JSSDK所有功能 (支持多公众号)
- 微信二维码推广支持 (支持多公众号)

	关注之后扫码回复支持多消息
	
## 其他开源项目

微信公众号开源项目  
【[weixin_pro(多公众号支持有后台)](http://git.oschina.net/javen205/weixin_pro)】  【[weixin_guide(公众号开发指南)](http://git.oschina.net/javen205/weixin_guide)】
【[jfinal-weixin(微信开发SDK)](http://git.oschina.net/jfinal/jfinal-weixin)】
【[jfinal-qyweixin(企业微信开发SDK)](http://git.oschina.net/javen205/jfinal_qyweixin)】

支付宝、微信支付开源项目

【[Android 微信、支付App支付SDK](http://git.oschina.net/javen205/JPay)】 【[IJPay 让支付触手可及。实现微信、支付宝系列支付](http://git.oschina.net/javen205/IJPay)】
	
## 更多支持
- 交流群：[![QQ](http://pub.idqqimg.com/wpa/images/group.png)](https://jq.qq.com/?_wv=1027&k=47koFFR)(备注项目名称、进群发广告T)
- Email：javen205@126.com

<img src="http://img.blog.csdn.net/20170425211154361?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvenl3X2phdmE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" width = "200" alt="如果对你有帮助，请随意打赏支持" align=center />



## 鸣谢

- [JFinal](http://git.oschina.net/jfinal/jfinal)
- [Jfinal-weixin](http://git.oschina.net/jfinal/jfinal-weixin)
- [eova](http://git.oschina.net/eova/eova)


