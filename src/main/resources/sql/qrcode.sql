#sql("getAll")
		select #(columns) from wx_qrcode  order by update_time desc
#end

#sql("getQrcodeByTicket")
	select * from wx_qrcode  
	where  status = #para(status)
	and app_id = #para(app_id)
	and ticket = #para(ticket)
	order by update_time desc
#end

#sql("getQrcodeBySceneId")
	select * from wx_qrcode  
	where  status = #para(status)
	and app_id = #para(app_id)
	and scene_id = #para(scene_id)
	order by update_time desc
#end

#sql("getQrcodeBySceneStr")
	select * from wx_qrcode  
	where  status = #para(status)
	and app_id = #para(app_id)
	and scene_str = #para(scene_str)
	order by update_time desc
#end