以下内容参考JFinalClub感兴趣的可以加入jfinal.com
在此统一管理所有 sql，优点有：
1：避免在 JFinalClubConfig 中一个个添加 sql 模板文件
2：免除在实际的模板文件中书写 namespace，以免让 sql 定义往后缩进一层
3：在此文件中还可以通过 define 指令定义一些通用模板函数，供全局共享
   例如定义通用的 CRUD 模板函数

#namespace("admin")
#include("admin.sql")
#end

#namespace("config")
#include("config.sql")
#end

#namespace("keyword")
#include("keyword.sql")
#end

#namespace("submsg")
#include("submsg.sql")
#end



#namespace("menutype")
#include("menutype.sql")
#end

#namespace("custommenu")
#include("custommenu.sql")
#end

#namespace("matchrulemenu")
#include("matchrulemenu.sql")
#end

#namespace("material")
#include("material.sql")
#end


#namespace("wxuser")
#include("wxuser.sql")
#end

#namespace("redpack")
#include("redpack.sql")
#end

#namespace("authorize")
#include("authorize.sql")
#end

#namespace("qrcode")
#include("qrcode.sql")
#end

#namespace("replymessage")
#include("replymessage.sql")
#end
