package com.javen.weixin._common;

import com.javen.weixin.controller.CustomMenuController;
import com.javen.weixin.controller.JSSDKController;
import com.javen.weixin.controller.MaterialController;
import com.javen.weixin.controller.QrcodeController;
import com.javen.weixin.controller.SubscribeMsgController;
import com.javen.weixin.controller.WeiXinMsgController;
import com.javen.weixin.controller.WeiXinOauthController;
import com.jfinal.config.Routes;

public class WeiXinRoutes extends Routes {

	public void config() {
		setBaseViewPath("/WEB-INF/_view");
		add("/msg", WeiXinMsgController.class);
		add("/material", MaterialController.class,"/");
		add("/menu", CustomMenuController.class,"/");
		add("/oauth", WeiXinOauthController.class,"/");
		add("/jssdk", JSSDKController.class,"/");
		add("/qrcode", QrcodeController.class,"/");
		add("/subscribemsg", SubscribeMsgController.class,"/");
	}
}





