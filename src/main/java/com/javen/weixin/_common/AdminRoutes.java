package com.javen.weixin._common;

import com.javen.weixin.controller.AdminController;
import com.javen.weixin.controller.IndexController;
import com.jfinal.config.Routes;

public class AdminRoutes extends Routes {

	public void config() {
		setBaseViewPath("/WEB-INF/_view");
		add("/", IndexController.class);
		add("/admin", AdminController.class,"/");
	}


}





