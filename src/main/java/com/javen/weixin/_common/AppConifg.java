/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin._common;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.util.JdbcUtils;
import com.alibaba.druid.wall.WallFilter;
import com.javen.weixin.common.model._MappingKit;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.template.Engine;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import com.jfinal.weixin.sdk.jfinal.ApiInterceptor;
import com.jfinal.weixin.sdk.jfinal.MsgInterceptor;

/**
 * @author Javen 2017年6月4日
 */
public class AppConifg extends JFinalConfig {
	static Log log = Log.getLog(AppConifg.class);
	private static Prop p = loadProp();

	public static  Prop loadProp() {
		try {
			return PropKit.use("config_pro.properties");
		} catch (Exception e) {
			return PropKit.use("config.properties");
		}
	}

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(p.getBoolean("devMode", false));
		me.setEncoding("utf-8");
		me.setError404View("/WEB-INF/error/404.html");
		me.setError500View("/WEB-INF/error/500.html");
		// ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
		ApiConfigKit.setDevMode(me.getDevMode());
	}
	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add(new AdminRoutes());
		me.add(new WeiXinRoutes());
		
	}

	/**
     * 抽取成独立的方法，例于 _Generator 中重用该方法，减少代码冗余
     */
	public static DruidPlugin getDruidPlugin() {
		String jdbcUrl = p.get("jdbcUrl");
		String user = p.get("user");
		String password = p.get("password").trim();
System.out.println("load data source: "+jdbcUrl + " " + user + " " + password);
		return new DruidPlugin(jdbcUrl, user, password);
	}
	
	public static DruidPlugin createDruidPlugin() {
		
		// 配置druid数据连接池插件
		DruidPlugin dp = getDruidPlugin();
		// 配置druid监控
		dp.addFilter(new StatFilter());
		WallFilter wall = new WallFilter();
		wall.setDbType(JdbcUtils.MYSQL);
		dp.addFilter(wall);
		return dp;
	}
	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 配置ActiveRecord插件
		DruidPlugin druidPlugin = createDruidPlugin();
		me.add(druidPlugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		 // 方言
        arp.setDialect(new MysqlDialect());
        // 事务级别
        arp.setTransactionLevel(Connection.TRANSACTION_REPEATABLE_READ);
        // 统一全部默认小写
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        // 是否显示SQL
        arp.setShowSql(p.getBoolean("devMode", false));
        
        arp.setBaseSqlTemplatePath(PathKit.getRootClassPath() + "/sql");
        arp.addSqlTemplate("all_sqls.sql");
        _MappingKit.mapping(arp);
        me.add(arp);
        
      
	}

	@Override
	public void configInterceptor(Interceptors me) {
		
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctxPath"));

		// Druid监控
		DruidStatViewHandler dvh = new DruidStatViewHandler("/druid", new IDruidStatViewAuth() {

			@Override
			public boolean isPermitted(HttpServletRequest request) {
				return true;
			}
		});
		me.add(dvh);
	}
	
	@Override
	public void configEngine(Engine me) {

	}
	
	@Override
	public void afterJFinalStart() {
		log.info("afterJFinalStart...");
//		两种方式实现多公众号的支持 默认实现方式使用自定义拦截器 
//		具体实现参考/weixin_pro/src/main/java/com/javen/weixin/Interceptor/ConfigInterceptor.java
//		MsgInterceptor.setAppIdParser(new CustomAppIdParser("rmid"));
//		ApiInterceptor.setAppIdParser(new CustomAppIdParser("rmid"));
		super.afterJFinalStart();
	}
}
