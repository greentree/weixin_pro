package com.javen.weixin._common;

import com.javen.weixin.common.model.Config;
import com.javen.weixin.service.ConfigService;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.weixin.sdk.jfinal.AppIdParser;

public class CustomAppIdParser implements AppIdParser {
    private static final String DEFAULT_APP_ID_KEY = "appId";
    
    private final String appIdKey;
    
    static ConfigService cs = ConfigService.me;
    
    public CustomAppIdParser() {
        this.appIdKey = DEFAULT_APP_ID_KEY;
    }

    public CustomAppIdParser(String agentIdKey) {
        this.appIdKey = agentIdKey;
    }
    

	@Override
	public String getAppId(Invocation inv) {
		return getAppId(inv.getController());
	}

	@Override
	public String getAppId(Controller ctl) {
		String  rmid = ctl.getPara(appIdKey);
		//从数据库中查询配置
		Config config = cs.getConfigByRmid(rmid);
		cs.getApiConfig(config);
		return config.getAppId();
	}

}
