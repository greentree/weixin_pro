/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Custommenu;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 自定义菜单
 * @author Javen
 * 2017年6月18日
 */
public class CustomMenuService {
	public static final CustomMenuService me = new CustomMenuService();
	private final Custommenu dao = new Custommenu().dao();
	/**
	 * 根据appId以及pid查询自定义菜单
	 * @param appId
	 * @param pid 为0时为父节点
	 * @return
	 */
	public List<Custommenu> getMenueByAppId(String appId,int pid,int menu_group){
		Kv para = Kv.by("app_id",appId).set("pid",pid).set("status",1).set("menu_group",menu_group);
		SqlPara sqlPara = dao.getSqlPara("custommenu.getMenueByAppId",para);
		return dao.find(sqlPara);
	}
}
