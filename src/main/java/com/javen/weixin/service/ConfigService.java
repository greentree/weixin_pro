/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import com.javen.weixin.common.model.Config;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;

/**
 * @author Javen
 * 2017年6月11日
 */
public class ConfigService {
	public static final ConfigService me = new ConfigService();
	private final Config dao = new Config().dao();
	/**
	 * 根据随机数查询微信配置信息
	 * @param rmid
	 * @return
	 */
	public Config getConfigByRmid(String rmid){
		Kv para = Kv.by("rmid", rmid).set("status", 1);
		SqlPara sqlPara = dao.getSqlPara("config.getConfigByRmid",para);
		return dao.findFirst(sqlPara);
	}
	
	/**
	 * WeiXinMsgController.ConfigInterceptor 会使用到
	 * @param config
	 */
	public void getApiConfig(Config config) {
		ApiConfig ac = new ApiConfig();
		
		// 配置微信 API 相关常量
		ac.setToken(config.getToken());
		ac.setAppId(config.getAppId());
		ac.setAppSecret(config.getAppSecret());
		
		/**
		 *  是否对消息进行加密，对应于微信平台的消息加解密方式：
		 *  1：true进行加密且必须配置 encodingAesKey
		 *  2：false采用明文模式，同时也支持混合模式
		 */
		ac.setEncryptMessage(config.getEncrypt() == 1 ? true:false);
		ac.setEncodingAesKey(config.getEncryptKey());
		ApiConfigKit.putApiConfig(ac);
	}
}
