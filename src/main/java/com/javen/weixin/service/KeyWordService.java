/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Keyword;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * @author Javen
 * 2017年6月11日
 */
public class KeyWordService {
	public static final KeyWordService me = new KeyWordService();
	private final Keyword dao = new Keyword().dao();
	
	public Keyword getFirstKW(String app_id,String keyword){
		Kv para = Kv.by("key_word", keyword).set("status",1).set("app_id",app_id);
		SqlPara sqlPara = dao.getSqlPara("keyword.getKeyWordByKey",para);
		return dao.findFirst(sqlPara);
	}
	
	
	public List<Keyword> getKeyWord(String app_id,String keyword){
		Kv para = Kv.by("key_word", keyword).set("status",1).set("app_id",app_id);
		SqlPara sqlPara = dao.getSqlPara("keyword.getKeyWordByKey",para);
		return dao.find(sqlPara);
	}
	
	public List<Keyword> getKeyWord(String app_id,String keyword,int type){
		Kv para = Kv.by("key_word", keyword).set("type", type).set("status",1).set("app_id",app_id);
		SqlPara sqlPara = dao.getSqlPara("keyword.getKeyWordByKey",para);
		return dao.find(sqlPara);
	}
}
