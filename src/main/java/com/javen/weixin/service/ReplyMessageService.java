/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Replymessage;
import com.jfinal.kit.Kv;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 回复消息
 * @author Javen
 * 2017年7月15日
 */
public class ReplyMessageService {
	static Log log = Log.getLog(ReplyMessageService.class);
	public static final ReplyMessageService me = new ReplyMessageService();
	private final Replymessage dao = new Replymessage().dao();
	
	/**
	 * 根据id查询回复消息的集合
	 * @param ids
	 * @param appId
	 * @return
	 */
	public List<Replymessage> getReplymessageByIds(String[] ids, String appId){
		if (null !=ids && ids.length > 0) {
			StringBuffer idstr = new StringBuffer();
			for (String id : ids) {
				idstr.append(" or id = ").append(id);
			}
			Kv para = Kv.by("ids",idstr.toString()).set("app_id", appId).set("status",1);
			SqlPara sqlPara = dao.getSqlPara("replymessage.getReplymessageByIds", para);
			return dao.find(sqlPara);
		}
		return null;
	}

}
