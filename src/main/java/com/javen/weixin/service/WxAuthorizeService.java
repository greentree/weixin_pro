/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import com.javen.weixin.common.model.Authorize;
import com.jfinal.kit.Kv;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 授权配置信息
 * @author Javen
 * 2017年7月2日
 */
public class WxAuthorizeService {
	static Log log = Log.getLog(WxAuthorizeService.class);
	public static final WxAuthorizeService me = new WxAuthorizeService();
	private final Authorize dao = new Authorize().dao();
	
	/**
	 * 根据appId 查询授权配置信息
	 * @param appId
	 * @return
	 */
	public Authorize getByAppId(String appId){
		Kv para = Kv.by("app_id",appId).set("status",1);
		SqlPara sqlPara = dao.getSqlPara("authorize.getAuthorizeByAppId", para);
		return dao.findFirst(sqlPara);
	}
	
}
