/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Matchrulemenu;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * 个性化自定义菜单
 * @author Javen
 * 2017年6月24日
 */
public class MatchruleMenuService {
	public static final MatchruleMenuService me = new MatchruleMenuService();
	private final Matchrulemenu dao = new Matchrulemenu().dao();
	/**
	 * 根据appId查询个性化自定义菜单
	 * @param appId
	 * @return
	 */
	public List<Matchrulemenu> getMatchruleMenuByAppId(String appId){
		Kv para = Kv.by("app_id",appId).set("status",1);
		SqlPara sqlPara = dao.getSqlPara("matchrulemenu.getByAppId",para);
		return dao.find(sqlPara);
	}
}
