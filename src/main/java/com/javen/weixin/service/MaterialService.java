/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Material;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * @author Javen
 * 2017年6月11日
 */
public class MaterialService {
	public static final MaterialService me = new MaterialService();
	private final Material dao = new Material().dao();
	
	public List<Material> getMaterialBySKI(String appId,String keyword){
		Kv para = Kv.by("app_id",appId).set("keyword",keyword).set("status",1);
		SqlPara sqlPara = dao.getSqlPara("material.getMaterialBySKI",para);
		return dao.find(sqlPara);
	}
	
	public Material getMaterialById(int id){
		return dao.findById(id);
	}
}
