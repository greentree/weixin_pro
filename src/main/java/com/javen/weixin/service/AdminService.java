/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Admin;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * @author Javen 2017年6月10日
 */
public class AdminService {
	public static final AdminService me = new AdminService();
	private final Admin dao = new Admin().dao();

	public Page<Admin> paginate(int pageNumber) {
		SqlPara sqlPara = dao.getSqlPara("admin.paginate");
		Page<Admin> projectPage = dao.paginate(pageNumber, 5, sqlPara);
		return projectPage;
	}

	public List<Admin> getAllAdmin(String columns) {
		Kv para = Kv.by("columns", columns).set("status", 1);
		SqlPara sqlPara = dao.getSqlPara("admin.getAllAdmin", para);
		return dao.find(sqlPara);
	}
}
