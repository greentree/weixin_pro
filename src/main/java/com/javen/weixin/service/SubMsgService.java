/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import java.util.List;

import com.javen.weixin.common.model.Submsg;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * @author Javen
 * 2017年6月11日
 */
public class SubMsgService {
	public static final SubMsgService me = new SubMsgService();
	private final Submsg dao = new Submsg().dao();
	
	public List<Submsg> getSubMsgByAppId(String appId){
		Kv para = Kv.by("app_id",appId).set("status",1);
		SqlPara sqlPara = dao.getSqlPara("submsg.getSubMsgByAppId",para);
		return dao.find(sqlPara);
	}
}
