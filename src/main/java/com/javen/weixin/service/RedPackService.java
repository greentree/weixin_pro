/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.service;

import com.javen.weixin.common.model.Redpack;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * @author Javen
 * 2017年6月12日
 */
public class RedPackService {
	public static final RedPackService me = new RedPackService();
	private final Redpack dao = new Redpack().dao();
	
	public Redpack getRedPackById(int id){
		Kv para = Kv.by("id",id);
		SqlPara sqlPara = dao.getSqlPara("redpack.getRedPackById",para);
		return dao.findFirst(sqlPara);
	}
}
