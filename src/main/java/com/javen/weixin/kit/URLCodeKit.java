package com.javen.weixin.kit;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class URLCodeKit {
	/**
	 *decode 解码
	 * @param decodeStr
	 * @return
	 */
	public static String decode(String decodeStr){
		try {
			String decode = URLDecoder.decode(decodeStr, "UTF-8");
			return decode;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decodeStr;
	}
	/**
	 * encode 编码
	 * @param encodeStr
	 * @return
	 */
	public static String encode(String encodeStr){
		try {
			String encode = URLEncoder.encode(encodeStr, "UTF-8");
			return encode;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodeStr;
	}
}
