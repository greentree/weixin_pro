/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.controller;

import java.util.List;

import com.javen.weixin.common.model.User;
import com.javen.weixin.kit.URLCodeKit;
import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.NotAction;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * @author Javen
 * 2017年6月4日
 */
public class IndexController extends Controller {
	@ActionKey("/user")
	public void test(){
		List<Record> users = Db.find("select * from wx_admin");
		setAttr("users", users);
		render("user.html");
	}
	
	public void index(){
		print("欢迎使用.  -By Javen");
	}
	
	@Before(NotAction.class)
	public void print(String str){
		renderText(str);
	}
	
	public void nickname(){
		try {
			List<User> users = new User().dao().find("select * from wx_user");
			for (User user : users) {
				user.setNickName(URLCodeKit.decode(user.getNickName()));
			}
			setAttr("users", users);
			render("wxuser.html");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
