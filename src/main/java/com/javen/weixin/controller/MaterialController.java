/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.controller;

import com.javen.weixin.common.model.Material;
import com.javen.weixin.service.MaterialService;
import com.jfinal.core.Controller;

/**
 * 
 * @author Javen
 * 2017年6月17日
 */
public class MaterialController extends Controller {
	
	MaterialService ms = MaterialService.me;
	/**
	 * 通过Id查询文章
	 */
	public void getById(){
		Integer id = getParaToInt("id");
		Material material = ms.getMaterialById(id);
		setAttr("material", material);
		render("article.html");
	}
	
}
