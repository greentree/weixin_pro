package com.javen.weixin.controller;

import com.javen.weixin.Interceptor.JSSDKInterceptor;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * @author Javen
 * 2016年5月13日
 */
/**
 * 对整个Controller或者其中的方法添加JSSDK签名验证拦截器
 */
@Before(JSSDKInterceptor.class)
public class JSSDKController extends Controller{
	
	public void index(){
		render("jssdk.html");
	}
	@Clear
	public void jsdk(){
		render("jssdk.html");
	}
	
}
