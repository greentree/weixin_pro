package com.javen.weixin.controller;

import java.io.UnsupportedEncodingException;

import com.SubscribeMsgApi;
import com.jfinal.core.Controller;
import com.jfinal.weixin.sdk.api.ApiResult;

public class SubscribeMsgController extends Controller{
	
	public void index(){
		try {
			String authorizeURL = SubscribeMsgApi.getAuthorizeURL("appId","123"
					, "模板ID"
					, "http://域名/subscribemsg/callBack", "123");
			System.out.println(authorizeURL);
			redirect(authorizeURL);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void send(){
		ApiResult subscribe = SubscribeMsgApi.subscribe("oKovkw5pRgaz8QhM5oocT3mfKYiA",
				"模板", "http://www.qq.com",
				123, "一次性订阅消息测试", "测试一次性订阅消息 -By Javen", "#111111");
		
		renderText(subscribe.getJson());
	}
	
	public void callBack(){
		String openid = getPara("openid");
		String template_id = getPara("template_id");
		String action = getPara("action");
		String scene = getPara("scene");
		
		System.out.println(openid+" "+template_id+" "+action+" "+scene);
		renderText(openid+" "+template_id+" "+action+" "+scene);
	}
}
