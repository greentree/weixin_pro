/**
 * Copyright (c) 2015-2017, Javen Zhou  (javen205@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.javen.weixin.controller;

import java.util.List;

import com.javen.weixin.common.model.Admin;
import com.javen.weixin.service.AdminService;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author Javen
 * 2017年6月10日
 */
public class AdminController extends Controller {
	static AdminService srv = AdminService.me;
	
	public void index() {
		Page<Admin> paginate = srv.paginate(getParaToInt("p", 1));
//		setAttr("users", paginate);
//		render("user.html");
		renderJson(paginate);
	}
	
	public void getAll(){
		List<Admin> allAdmin = srv.getAllAdmin(" * ");
		setAttr("users", allAdmin);
		render("user.html");
	}
}
